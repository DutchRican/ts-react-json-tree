# TypeScript React JSON tree
##ts-react-json-tree

### Another json viewer, why?
I wanted to have a tree-view that can highlight items, expand items and link to other pages from external input.

### Example View
![Imgur](https://i.imgur.com/prTuoMT.png)
### How does it work?

##### Simple manual example:
```import JsonTree from 'ts-react-json-tree';

<JsonTree
        data={data}
        minLevel={1}
        linkLocation="/some/location"
    />
```

#### External managing of expanded items
```import JsonTree from 'ts-react-json-tree';

<JsonTree
        data={data}
        minLevel={1}
        expandedItems={this.props.expandedItems}
        highlightedItems={this.props.highlightedItems}
        clickHandler={item => this.props.setExpandedItems(item)}
        linkLocation="/some/location"
    />
```

<b>The props should preferably come from redux</b>

### CSS 
CSS is included in the tree and prefixed with `tsjt`

### LinkFormat
Tree key: `$[0]['balance']` with the value: `$2,887.03`
Pass the base_url, the values are added as:
`base_url?key=$[0][%27balance%27]&value=%242%2C887.03`

### data-status
Objects and arrays have the `data-status` attribute that should help expanding multiple items at once.