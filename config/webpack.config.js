const path = require('path');

const config = {
  entry: ['./test/index.tsx'],
  devServer: {
    port: 3000,
    contentBase: path.join(__dirname, '..', 'test')
  },
  output: {
    path: path.join(__dirname, '..', 'dist'),
    filename: 'index.js',
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.jsx', '.json']
  },

  module: {
    rules: [
      {
        test: /\.tsx?$/,
        loader: 'ts-loader',
        exclude: /node_modules/
      },
      {
        test: /\.css$/,
        use: [ 'style-loader', 'css-loader' ]
      }
    ]
  }
};

module.exports = config;

