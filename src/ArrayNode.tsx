import * as React from 'react';
import { NodeSelector } from './NodeSelector';
import { PlusIcon } from './icons/PlusIcon';
import { MinusIcon } from './icons/MinusIcon';
import { OpenBracket, CloseBracket, pathBuild } from './helper';
import { withContext } from './ContextWrapped';

interface propTypes {
    items: any[],
    depth: number,
    dataPath: string[],
    minLevel: number,
    expandedItems?: string[],
    highlightedItems?: Array<any>,
    clickHandler: Function
};

export class ArrayNode extends React.Component<propTypes, any> {
    constructor(props: propTypes) {
        super(props);
        this.state = {
            collapsed: props.depth > props.minLevel,
            handleExpand: this.handleExpand.bind(this),
            userOverrideCollapse: false,
            itemCount: props.items.length
        };
    }

    static getDerivedStateFromProps(nextProps: any, prevState: any): {} {
        if (nextProps.expandedItems.includes(pathBuild(nextProps.dataPath)) && prevState.userOverrideCollapse === false) {
            return { collapsed: false };
        }
        return null;
    }

    handleExpand() {
        this.setState({ collapsed: !this.state.collapsed, userOverrideCollapse: true });
    }

    render() {
        const { depth, highlightedItems, clickHandler } = this.props;
        const newDepth = depth + 1;
        let content;
        if (!this.state.collapsed) {
            content = this.props.items.map((el, index) => {
                const dataPath = [...this.props.dataPath];
                dataPath.push(`[${index}]`);
                const attrib = pathBuild(dataPath);
                const highlighted = highlightedItems.find(el => el.path === attrib);
                const newProps = { ...this.props, value: el, depth: newDepth, dataPath }
                return <li key={`${el}-${index}`}
                    className="tsjt_li"
                    data-status={`expanded-${depth}`}
                    data-attrib={pathBuild(dataPath)}
                    style={highlighted ? { backgroundColor: highlighted.color } : {}}>
                    <NodeSelector {...newProps} />
                </li>
            });
        }

        return (
            <React.Fragment>
                <span className="tsjt_collapser"
                onClick={clickHandler ? () => clickHandler(pathBuild(this.props.dataPath)) : this.state.handleExpand}>{this.state.collapsed ? <PlusIcon color="#1a8b0b" /> : <MinusIcon color="#F00" />}</span>
                <OpenBracket symbol='[' depth={newDepth} />
                {content 
                    ? <ul className="tsjt_Array">{content}</ul> 
                    : <span onClick={this.state.handleExpand} className="tsjt_collapser_small" data-status={`collapsed-${depth}`}>…</span>}
                <CloseBracket symbol=']' depth={newDepth} />
            </React.Fragment>
        );
    }
};

export default withContext(ArrayNode);
