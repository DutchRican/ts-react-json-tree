import * as React from 'react';
import { JsonTreeCtx } from '.';

export const withContext = (WrappedComponent: any) => {
  return class extends React.Component<any, any> {
    render() {
      return (
        <JsonTreeCtx.Consumer>
          {value => (
            <WrappedComponent {...this.props} 
            expandedItems={value.expandedItems} 
            highlightedItems={value.highlightedItems} 
            linkLocation={value.linkLocation}
            clickHandler={value.clickHandler}
            />
          )}
        </JsonTreeCtx.Consumer>
      );
    }
  }
}