import * as React from 'react';
import { SpanNode } from './SpanNode';
import { LinkIcon } from './icons/LinkIcon';
import { pathBuild } from './helper';
import { withContext } from './ContextWrapped';

interface propTypes {
    value: any,
    depth: number,
    dataPath: string[],
    linkLocation?: string,
    type: string
};

export const EndNode = (props: propTypes) => {
    const { value, depth, linkLocation, dataPath, type } = props;
    const endValue = type === 'string' ? `"${value}"` : value;
    return <React.Fragment><SpanNode value={endValue} attribs={{ className: `tsjt_${type}` }} depth={depth} />
        {linkLocation && <a href={`${linkLocation}?key=${pathBuild(dataPath)}&value=${encodeURIComponent(value)}`}><LinkIcon color="blue"/></a>}
    </React.Fragment>
};


export default withContext(EndNode);
