import * as React from 'react';
import EndNode from './EndNode';
import {default as ObjectNode} from './ObjectNode';
import { default as ArrayNode } from './ArrayNode';

interface NodeSelectorProps {
  value: any,
  depth?: number,
  minLevel?: number,
  dataPath?: string[],
  base?: string
};

export const NodeSelector = ({ value, depth = 0, minLevel = 0, dataPath = [], base = '$'}: NodeSelectorProps) => {
  if (!dataPath.length) dataPath.push(base);
  const props = { items: value, depth, minLevel, dataPath};
  if (value !== null) {
    var type = typeof value;
    switch (type) {
      case 'object':
        if (value instanceof Array) {
          return <ArrayNode {...props} />;
        } else {
          return <ObjectNode {...props} />;
        }
      default: 
        return <EndNode value={value} depth={depth} dataPath={dataPath} type={type} />
    }
  } else {
    return <EndNode value={value} depth={depth} dataPath={dataPath} type={null} />;
  }
};
