import * as React from 'react';
import { NodeSelector } from './NodeSelector';
import { PlusIcon } from './icons/PlusIcon';
import { MinusIcon } from './icons/MinusIcon';
import { withContext } from './ContextWrapped';
import { OpenBracket, CloseBracket, pathBuild } from './helper';
import { isNumber } from 'util';

interface propTypes {
  items: any,
  depth: number,
  minLevel: number,
  dataPath: string[],
  expandedItems?: string[],
  highlightedItems?: Array<any>,
  clickHandler?: Function
};

export class ObjectNode extends React.Component<propTypes, any> {

  constructor(props: propTypes) {
    super(props);
    this.state = {
      collapsed: props.depth > props.minLevel,
      userOverrideCollapse: false,
      handleExpand: this.handleExpand.bind(this)
    };
  }

  static getDerivedStateFromProps(nextProps: any, prevState: any): {} {
    const newState: any = {};
    if (nextProps.expandedItems.includes(pathBuild(nextProps.dataPath)) && prevState.userOverrideCollapse === false) {
      newState.collapsed = false;
    }
    if (nextProps.highlightedItems.includes(pathBuild(nextProps.dataPath))) {
      newState.highlighted = true;
    }
    return Object.keys(newState) ? newState : null;
  }

  handleExpand() {
    this.setState({ collapsed: !this.state.collapsed, userOverrideCollapse: true });
  }

  render() {
    const { items, depth, minLevel, highlightedItems, clickHandler } = this.props;
    let content: any;
    if (!this.state.collapsed) {
      content = Object.keys(items).map((el, index) => {
        const dataPath = [...this.props.dataPath];
        const pathElement = isNumber(el) ? `[${el}]` : `['${el}']`;
        dataPath.push(pathElement);
        const attrib = pathBuild(dataPath);
        const highlighted = highlightedItems.find(el => el.path === attrib);
        return <li
          key={`${el}-${index}`}
          className="tsjt_li"
          data-status={`expanded-${depth}`}
          data-attrib={attrib}
          style={highlighted ? { backgroundColor: highlighted.color } : {}}>
          <span>{`"${el}"`}</span><span className="tsjt_spacer">:</span>
          <NodeSelector value={items[el]} depth={depth + 1} dataPath={dataPath} minLevel={minLevel} />
        </li>
      });
    }

    return (
      <React.Fragment>
        <span className="tsjt_collapser"
          onClick={clickHandler ? () => clickHandler(pathBuild(this.props.dataPath)) : this.state.handleExpand}>{this.state.collapsed ? <PlusIcon color="#1a8b0b" /> : <MinusIcon color="#F00" />}
        </span>
        <OpenBracket symbol='{' depth={depth + 1} />
        {content 
          ? <ul className="tsjt_Object">{content}</ul> 
          : <span onClick={this.state.handleExpand} className="tsjt_collapser_small" data-status={`collapsed-${depth}`}>…</span>}
        <CloseBracket symbol='}' depth={depth + 1} />
      </React.Fragment>
    );
  }
};

export default withContext(ObjectNode);
