import * as React from 'react';

interface propTypes {
  value: any,
  attribs: {},
  depth: number,
}

export const SpanNode = (props: propTypes) => {
  return <span {...props.attribs}>{`${props.value}`}</span>
};