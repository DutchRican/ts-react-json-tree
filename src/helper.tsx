import * as React from 'react';
import { SpanNode } from './SpanNode';

export const pathBuild = (pathArray: string[]) => {
  return pathArray.join('');
}

interface OpenBracketProps {
  symbol: any,
  depth: number,
}

export const OpenBracket = (props: OpenBracketProps) => {
  return (
      <SpanNode
        value={props.symbol}
        attribs={{ className: 'jstBracket' }}
        depth={props.depth}/>
  );
};

interface CloseBracketProps {
  symbol: any,
  depth: number
};

export const CloseBracket = (props: CloseBracketProps) => {
  return <SpanNode
    value={props.symbol}
    attribs={{}} depth={props.depth} />;
};
