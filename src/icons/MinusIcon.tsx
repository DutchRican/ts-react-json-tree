import * as React from 'react';

export const MinusIcon = ({color}:{color:string}) => {
  return (<div className="tsjt_svg_container">
    <svg className="tsjt_svg" viewBox="0 0 100 100" stroke={color}>
      <g>
        <circle className="tsjt_svg_circle" cx={50} cy={50} r={25}></circle>
        <line className="tsjt_svg_line" x1={30} y1={50} x2={70} y2={50}></line>
      </g>
    </svg>
  </div>
  );
};