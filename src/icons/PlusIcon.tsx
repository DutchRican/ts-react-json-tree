import * as React from 'react';

export const PlusIcon = ({color}:{color:string}) => {
  return (<div className="tsjt_svg_container">
    <svg className="tsjt_svg" viewBox="0 0 100 100" stroke={color}>
      <g>
        <circle className="tsjt_svg_circle" cx={50} cy={50} r={25}></circle>
        <line className="tsjt_svg_line" x1={30} y1={50} x2={70} y2={50}></line>
        <line className="tsjt_svg_line" x1={50} y1={30} x2={50} y2={70}></line>
      </g>
    </svg>
  </div>
  );
};
