import * as React from 'react';
import { NodeSelector } from './NodeSelector';
import './tsjt_base.css';

interface propTypes {
  data: any,
  base?: string,
  minLevel?: number
  expandedItems?: string[],
  highlightedItems?: Array<any>,
  linkLocation?: string,
  clickHandler?: Function
};

interface ctxInt {
  expandedItems: string[],
  highlightedItems: Array<any>,
  linkLocation: string,
  clickHandler?: Function
};

export const JsonTreeCtx = React.createContext<ctxInt | null>(null);

export class JsonTree extends React.Component<propTypes, {}> {
  render() {
    const props = { ...this.props, value: this.props.data };
    return (
      <JsonTreeCtx.Provider value={{
        expandedItems: this.props.expandedItems,
        highlightedItems: this.props.highlightedItems,
        linkLocation: this.props.linkLocation,
        clickHandler: this.props.clickHandler
      }}>
        <div className="tsjt_base"><NodeSelector {...props} /></div>
      </JsonTreeCtx.Provider >
    );
  }
}

export default JsonTree;
