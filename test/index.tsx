/***  examples/src/index.js ***/
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import JsonTree from '../src/';


const data = require('./fake.json');
class App extends React.Component<any, any> {
  state = {
    expandedItems: ["$[0]['tags']", "$[0]['tags'][5]", "$[0]['name']"]
  }
  render() {
    const { expandedItems } = this.state;
    return (
      <div style={{ width: '50%' }}>
        <JsonTree
          data={data}
          minLevel={1}
          expandedItems={expandedItems}
          highlightedItems={[{ path: "$[0]['tags'][4]", color: '#fe2' }, { path: "$[0]['greeting']", color: '#1f1' }]}
          linkLocation="/some/location"
          clickHandler={(e: any) => this.setState({ expandedItems: [...expandedItems, e]})}
        />
      </div>
    )
  }
}

ReactDOM.render(<App />, document.getElementById("root"));